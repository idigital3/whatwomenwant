﻿using System;
using WhatWomenWant;
using WhatWomenWant.iOS.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(MyWebView), typeof(WebViewCustom))]
namespace WhatWomenWant.iOS.Renderers
{
    public class WebViewCustom : WebViewRenderer
    {
        protected override void OnElementChanged(VisualElementChangedEventArgs e)
        {
            base.OnElementChanged(e);

            var webView = this;
            webView.ScrollView.Bounces = false;
        }
    }
}
