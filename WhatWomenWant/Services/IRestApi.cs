﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Refit;
using WhatWomenWant.Models;

namespace WhatWomenWant
{
    public interface IRestApi
    {

        [Get("/config_json.php")]
        Task<RootObject> GetData();


    }
}
