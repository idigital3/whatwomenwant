﻿using System;
namespace WhatWomenWant.Services
{
    public interface IAppVersion
    {
        string GetVersion();
        string GetBuild();
    }
}
