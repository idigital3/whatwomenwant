﻿using System;
using System.Threading.Tasks;
using Refit;
using System.Collections.Generic;
using WhatWomenWant.Models;

namespace WhatWomenWant
{
    public class RestApi
    {
        public const string BASE_URL_REST_API = "https://m.wwwshopping.it/";
        internal static IRestApi RefitApi { get; private set; }

        static RestApi()
        {
            RefitApi = Refit.RestService.For<IRestApi>(BASE_URL_REST_API);
        }

        internal static async Task<RootObject> GetData()
        => await RefitApi.GetData();


        /*internal static async Task<Messaggio> NuovoMessaggioAziendale(string messaggio, int chatId) 
            => await RefitApi.NuovoMessaggioAziendale(messaggio, chatId);

        internal static async Task<List<Messaggio>> ListaMessaggi(int chatId)
            => await RefitApi.ListaMessaggi(chatId);*/


    }
}
