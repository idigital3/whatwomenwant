﻿using System;
using System.Collections.Generic;
using WhatWomenWant.Models;
using WhatWomenWant.Utility;
using Xamarin.Forms;
using System.Linq;

namespace WhatWomenWant
{
    [ContentProperty("ContainerContent")]
        
    public partial class MyView : ContentView
    {
        RootObject oggetto;
        MyWebView web_view;
        Label titolo_sezione;

        public MyView()
        {
            InitializeComponent();

            var tapGesture = new Xamarin.Forms.TapGestureRecognizer();
            tapGesture.Tapped += TapGesture_Tapped;

            blocco_1.GestureRecognizers.Add(tapGesture);
            blocco_2.GestureRecognizers.Add(tapGesture);
            blocco_3.GestureRecognizers.Add(tapGesture);
            blocco_4.GestureRecognizers.Add(tapGesture);
            blocco_5.GestureRecognizers.Add(tapGesture);
            blocco_6.GestureRecognizers.Add(tapGesture);
            blocco_7.GestureRecognizers.Add(tapGesture);
            blocco_8.GestureRecognizers.Add(tapGesture);
            blocco_9.GestureRecognizers.Add(tapGesture);
            blocco_10.GestureRecognizers.Add(tapGesture);

            ContentSilver.GestureRecognizers.Add(tapGesture);
        }



        public View ContainerContent
        {
            get { return ContentFrame.Content; }
            set { ContentFrame.Content = value; }
        }


        public bool MenuVisible
        {
            get 
            {
                return this.IsVisible;
            }
            set 
            {
                this.IsVisible = true;

                if(value)
                    this.FadeTo(1, 300);
                else
                    this.FadeTo(0, 300);

                this.IsVisible = value;
            }
        }


        public string NomeUtente
        {
            get { return this.l_nome.Text; }
            set { this.l_nome.Text = value; }
        }


        public void Init(RootObject oggetto,MyWebView web_view,Label titolo_sezione)
        {
            this.oggetto = oggetto;
            this.web_view = web_view;
            this.titolo_sezione = titolo_sezione;


            l_benvenuto.Text = oggetto.top_menu.profilo.welcome;

            if (!string.IsNullOrWhiteSpace(Sistema.UserName))
                l_nome.Text = Sistema.UserName;

            if(!string.IsNullOrWhiteSpace(Sistema.URL_DOMINIO + oggetto.top_menu.profilo.logo))
               img_logo.Source = Sistema.URL_DOMINIO + oggetto.top_menu.profilo.logo;

            List<Link> listaVociMenu = oggetto.top_menu.profilo.link.OrderBy(x=>x.enabled == true).ToList();


            if (listaVociMenu[0].enabled)
            {
                l_menu_1.Text = listaVociMenu[0].label;
                blocco_1.IsVisible = true;
            }
            if (listaVociMenu[1].enabled)
            {
                l_menu_2.Text = listaVociMenu[1].label;
                blocco_2.IsVisible = true;
            }
            if (listaVociMenu[2].enabled)
            {
                l_menu_3.Text = listaVociMenu[2].label;
                blocco_3.IsVisible = true;
            }
            if (listaVociMenu[3].enabled)
            {
                l_menu_4.Text = listaVociMenu[3].label;
                blocco_4.IsVisible = true;
            }
            if (listaVociMenu[4].enabled)
            {
                l_menu_5.Text = listaVociMenu[4].label;
                blocco_5.IsVisible = true;
            }
            if (listaVociMenu[5].enabled)
            {
                l_menu_6.Text = listaVociMenu[5].label;
                blocco_6.IsVisible = true;
            }
            if (listaVociMenu[6].enabled)
            {
                l_menu_7.Text = listaVociMenu[6].label;
                blocco_7.IsVisible = true;
            }
            if (listaVociMenu[7].enabled)
            {
                l_menu_8.Text = listaVociMenu[7].label;
                blocco_8.IsVisible = true;
            }
            if (listaVociMenu[8].enabled)
            {
                l_menu_9.Text = listaVociMenu[8].label;
                blocco_9.IsVisible = true;
            }
            if (listaVociMenu[9].enabled)
            {
                l_menu_10.Text = listaVociMenu[9].label;
                blocco_10.IsVisible = true;
            }



        }


        void TapGesture_Tapped(object sender, EventArgs e)
        {
            string nome_componente = sender.GetType().Name;

            if (nome_componente.Equals("StackLayout"))
            {
                StackLayout stack = (StackLayout)sender;
                string class_name = stack.ClassId;

                List<Link> listaVociMenu = oggetto.top_menu.profilo.link.OrderBy(x => x.enabled == true).ToList();

                string url = "";
                string label = "";


                if (class_name.Equals("blocco_1"))
                {
                    url = listaVociMenu[0].url;
                    label = listaVociMenu[0].label;
                    titolo_sezione.Text = listaVociMenu[0].navbar_title;
                }
                if (class_name.Equals("blocco_2"))
                {
                    url = listaVociMenu[1].url;
                    label = listaVociMenu[1].label;
                    titolo_sezione.Text = listaVociMenu[1].navbar_title;
                }
                if (class_name.Equals("blocco_3"))
                {
                    url = listaVociMenu[2].url;
                    label = listaVociMenu[2].label;
                    titolo_sezione.Text = listaVociMenu[2].navbar_title;
                }
                if (class_name.Equals("blocco_4"))
                {
                    url = listaVociMenu[3].url;
                    label = listaVociMenu[3].label;
                    titolo_sezione.Text = listaVociMenu[3].navbar_title;
                }
                if (class_name.Equals("blocco_5"))
                {
                    url = listaVociMenu[4].url;
                    label = listaVociMenu[4].label;
                    titolo_sezione.Text = listaVociMenu[4].navbar_title;
                }
                if (class_name.Equals("blocco_6"))
                {
                    url = listaVociMenu[5].url;
                    label = listaVociMenu[5].label;
                    titolo_sezione.Text = listaVociMenu[5].navbar_title;
                }
                if (class_name.Equals("blocco_7"))
                {
                    url = listaVociMenu[6].url;
                    label = listaVociMenu[6].label;
                    titolo_sezione.Text = listaVociMenu[6].navbar_title;
                }
                if (class_name.Equals("blocco_8"))
                {
                    url = listaVociMenu[7].url;
                    label = listaVociMenu[7].label;
                    titolo_sezione.Text = listaVociMenu[7].navbar_title;
                }
                if (class_name.Equals("blocco_9"))
                {
                    url = listaVociMenu[8].url;
                    label = listaVociMenu[8].label;
                    titolo_sezione.Text = listaVociMenu[8].navbar_title;
                }
                if (class_name.Equals("blocco_10"))
                {
                    url = listaVociMenu[9].url;
                    label = listaVociMenu[9].label;
                    titolo_sezione.Text = listaVociMenu[9].navbar_title;
                }

                if (label.ToLower() == "logout")
                {
                    titolo_sezione.Text = "";
                    Sistema.Logout(this);
                }


                if (!string.IsNullOrWhiteSpace(url) && !string.IsNullOrWhiteSpace(url.Split('.')[0]))
                    web_view.Source = Sistema.URL_DOMINIO + url;

                MenuVisible = false;
            }
            else if(nome_componente.Equals("Frame"))
            {
                Frame frame = (Frame)sender;
                string class_name = frame.ClassId;

                if (class_name.Equals("ContentSilver"))
                {
                    MenuVisible = false;
                }
            }
        }

    }
}
