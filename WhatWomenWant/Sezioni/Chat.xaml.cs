﻿using System;
using System.Collections.Generic;
using WhatWomenWant.Models;
using WhatWomenWant.Services;
using WhatWomenWant.Utility;
using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration.iOSSpecific;

namespace WhatWomenWant.Sezioni
{
    public partial class Chat : ContentPage
    {
        RootObject oggetto;
        string currentUrl;

        public Chat(RootObject oggetto)
        {
            InitializeComponent();

            Xamarin.Forms.NavigationPage.SetHasNavigationBar(this, false);
            On<Xamarin.Forms.PlatformConfiguration.iOS>().SetUseSafeArea(true);

            this.oggetto = oggetto;


            var tapGesture = new Xamarin.Forms.TapGestureRecognizer();
            tapGesture.Tapped += TapGesture_Tapped; ;

            stack_profilo.GestureRecognizers.Add(tapGesture);
            img_back.GestureRecognizers.Add(tapGesture);

            web_view.Navigated += Web_View_Navigated;
            web_view.Navigating += Web_View_Navigating;


            //if (!string.IsNullOrWhiteSpace(Sistema.UserName))
            //{
                //loading.IsVisible = true;
                //web_view.IsVisible = false;

            //web_view.Source = Sistema.URL_DOMINIO + "api/?action=checkToken&token=" + Sistema.TokenUtente;
            //Sistema.CheckToken(web_view, Sistema.URL_DOMINIO + "api/?action=checkToken&token=" + Sistema.TokenUtente, view_menu, oggetto.tapbar[3].url);
            //}

            //web_view.Source = Sistema.URL_DOMINIO + oggetto.tapbar[3].url;

            view_menu.Init(oggetto, web_view, l_titolo);
        }


        public Chat()
        {

        }


        protected override async void OnAppearing()
        {
            base.OnAppearing();

            view_menu.MenuVisible = false;

            l_titolo.Text = oggetto.tapbar[3].navbar_title;

            if (Sistema.AggiornamentoRichiesto(oggetto))
            {
                await DisplayAlert("Attenzione", "Per continuare è necessario aggiornare l'applicazione all'ultima versione", "OK");
                return;
            }

            loading.IsVisible = true;
            web_view.IsVisible = false;
            web_view.Source = Sistema.URL_DOMINIO + oggetto.tapbar[3].url + "?token=" + Sistema.TokenUtente + "&device=" + Device.RuntimePlatform.ToLower() + "&versione=" + DependencyService.Get<IAppVersion>().GetBuild();

            /*if (string.IsNullOrWhiteSpace(Sistema.UserName))
            {
                loading.IsVisible = true;
                web_view.IsVisible = false;

                //web_view.Source = Sistema.URL_DOMINIO + "api/?action=checkToken&token=" + Sistema.TokenUtente;
                Sistema.CheckToken(web_view, Sistema.URL_DOMINIO + "api/?action=checkToken&token=" + Sistema.TokenUtente, view_menu, oggetto.tapbar[3].url);
            }*/
        }


        void Web_View_Navigated(object sender, WebNavigatedEventArgs e)
        {
            currentUrl = e.Url;

            if (!e.Url.Contains(Sistema.URL_DOMINIO + "api/"))
            {
                loading.IsVisible = false;
                web_view.IsVisible = true;
            }


            //Sistema.CheckToken(web_view, e.Url, view_menu, oggetto.tapbar[3].url);

            Sistema.VerificaLogin(web_view, e.Url, view_menu);
        }


        void Web_View_Navigating(object sender, WebNavigatingEventArgs e)
        {
            loading.IsVisible = true;
            web_view.IsVisible = false;
        }


        void TapGesture_Tapped(object sender, EventArgs e)
        {
            string nome_componente = sender.GetType().Name;

            if (nome_componente.Equals("StackLayout"))
            {
                StackLayout stack = (StackLayout)sender;
                string class_name = stack.ClassId;
                if (class_name.Equals("stack_profilo") && !Sistema.AggiornamentoRichiesto(oggetto))
                    view_menu.MenuVisible = !view_menu.MenuVisible;
            }
            else if (nome_componente.Equals("Image"))
            {
                Image image = (Image)sender;
                string class_name = image.ClassId;

                if (class_name.Equals("img_back"))
                {
                    if (web_view.CanGoBack && currentUrl != Sistema.URL_DOMINIO + oggetto.tapbar[0].url)
                    {
                        web_view.IsVisible = false;
                        web_view.GoBack();
                    }
                }
            }


        }
    }
}
