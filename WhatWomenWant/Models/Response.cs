﻿using System;
namespace WhatWomenWant.Models
{
    public class Response
    {
        public string status { get; set; }
        public string message { get; set; }
        public string nome { get; set; }
        public string cognome { get; set; }
        public string id_utente { get; set; }
        public string token { get; set; }
        public string go_to { get; set; }
        public string device { get; set; }
        public string debug { get; set; }
    }
}
