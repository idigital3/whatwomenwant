﻿using System;
namespace WhatWomenWant.Models
{
    public class TapBar
    {
        public string label { get; set; }
        public string url { get; set; }
        public bool required_login { get; set; }
        public string navbar_title { get; set; }
    }
}
