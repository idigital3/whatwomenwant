﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WhatWomenWant.Models;
using WhatWomenWant.Sezioni;
using WhatWomenWant.Utility;
using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration.iOSSpecific;

namespace WhatWomenWant
{
    public partial class MainPage : TabbedPage
    {
        RootObject oggetto;

        public MainPage()
        {
            InitializeComponent();

            Xamarin.Forms.NavigationPage.SetHasNavigationBar(this, false);
            On<Xamarin.Forms.PlatformConfiguration.iOS>().SetUseSafeArea(true);



            Init();
        }




        async void Init()
        {
            try
            {
                oggetto = await RestApi.GetData();
                
                if (oggetto == null)
                {
                    await DisplayAlert("Attenzione", "Si è verificato un errore", "OK");
                    return;
                }


                HomePage homePage = new HomePage(oggetto);
                homePage.Title = oggetto.tapbar[0].label;
                homePage.Icon = "home.png";
                this.Children.Add(homePage);

                Collezione collezione = new Collezione(oggetto);
                collezione.Title = oggetto.tapbar[1].label;
                collezione.Icon = "collezione.png";
                this.Children.Add(collezione);

                Camerino camerino = new Camerino(oggetto);
                camerino.Title = oggetto.tapbar[2].label;
                camerino.Icon = "camerino.png";
                this.Children.Add(camerino);


                Chat chat = new Chat(oggetto);
                chat.Title = oggetto.tapbar[3].label;
                chat.Icon = "wws.png";
                this.Children.Add(chat);


                Contatti contatti = new Contatti(oggetto);
                contatti.Title = oggetto.tapbar[4].label;
                contatti.Icon = "contatti.png";
                this.Children.Add(contatti);

            }
            catch(Exception ex){

                await DisplayAlert("Attenzione", "Si è verificato un errore", "OK");
                return;
            }

        }

    }
}
