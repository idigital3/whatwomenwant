﻿using System;
using Android.App;
using Android.Content;
using Android.Provider;
using Java.IO;
using WhatWomenWant.Droid.Helpers;
using WhatWomenWant.Droid.Renderers;
using WhatWomenWant;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(MyWebView), typeof(WebViewCustom))]
namespace WhatWomenWant.Droid.Renderers
{
    public class WebViewCustom : WebViewRenderer
    {
        private static int FILECHOOSER_RESULTCODE = 1;

        protected override void OnElementChanged(ElementChangedEventArgs<WebView> e)
        {
            base.OnElementChanged(e);

            var chromeClient = new FileChooserWebChromeClient((uploadMsg, acceptType, capture) => {
                MainActivity.UploadMessage = uploadMsg;

                // Create MyAppFolder at SD card for saving our images
                File imageStorageDir = new File(Android.OS.Environment.GetExternalStoragePublicDirectory(
                        Android.OS.Environment.DirectoryPictures), "MyAppFolder");

                if (!imageStorageDir.Exists())
                {
                    imageStorageDir.Mkdirs();
                }

                // Create camera captured image file path and name, add ticks to make it unique 
                var file = new File(imageStorageDir + File.Separator + "IMG_"
                    + DateTime.Now.Ticks + ".jpg");

                MainActivity.mCapturedImageURI = Android.Net.Uri.FromFile(file);

                // Create camera capture image intent and add it to the chooser
                var captureIntent = new Intent(MediaStore.ActionImageCapture);
                captureIntent.PutExtra(MediaStore.ExtraOutput, MainActivity.mCapturedImageURI);

                var i = new Intent(Intent.ActionGetContent);
                i.AddCategory(Intent.CategoryOpenable);
                i.SetType("image/*");

                var chooserIntent = Intent.CreateChooser(i, "Choose image");
                chooserIntent.PutExtra(Intent.ExtraInitialIntents, new Intent[] { captureIntent });

                ((Activity)Forms.Context).StartActivityForResult(chooserIntent, FILECHOOSER_RESULTCODE);
            });

            Control.SetWebChromeClient(chromeClient);
        }
    }
}